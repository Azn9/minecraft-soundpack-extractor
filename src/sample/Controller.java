package sample;

import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonObject;
import com.jfoenix.controls.JFXButton;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicInteger;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

public class Controller {

    @FXML
    public JFXButton sourceSelectorButton;

    @FXML
    public JFXButton selectDestButton;

    @FXML
    public JFXButton startExportButton;

    private File       indexesFile;
    private JsonObject sourceFile;
    private File       outputDirectory;

    public void selectSource() {
        JFileChooser chooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter(".json file", "json");
        chooser.setFileFilter(filter);

        if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
            File file = chooser.getSelectedFile();
            if (file == null)
                return;

            this.indexesFile = file.getParentFile();

            String string = "";

            try {
                BufferedReader bufferedReader = new BufferedReader(new FileReader(file));

                string = bufferedReader.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (string.equals("")) {
                Alert alert = new Alert(AlertType.ERROR);
                alert.setTitle("Error");
                alert.setHeaderText(null);
                alert.setContentText("The selected file seems empty !");

                alert.showAndWait();
                return;
            }

            this.sourceFile = (JsonObject) Json.parse(string);

            if (this.sourceFile.get("objects") == null || this.sourceFile.get("objects").isNull() || this.indexesFile == null || !this.indexesFile.getName().equalsIgnoreCase("indexes")) {
                Alert alert = new Alert(AlertType.ERROR);
                alert.setTitle("Error");
                alert.setHeaderText(null);
                alert.setContentText("The selected file is not a valid minecraft asset file !");

                alert.showAndWait();

                this.sourceFile = null;
            }
        }
    }

    public void selectDestination() {
        JFileChooser chooser = new JFileChooser();
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

        if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
            File file = chooser.getSelectedFile();

            if (file == null) {
                Alert alert = new Alert(AlertType.ERROR);
                alert.setTitle("Error");
                alert.setHeaderText(null);
                alert.setContentText("The selected folder is invalid !");

                alert.showAndWait();
                return;
            }

            this.outputDirectory = file;
        }
    }

    public void startExport() {
        if (this.sourceFile == null || this.outputDirectory == null) {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Missing settings");
            alert.setHeaderText(null);
            alert.setContentText("Please check you successfully selected the input file and the output folder !");

            alert.showAndWait();

            return;
        }

        JsonObject objects = (JsonObject) this.sourceFile.get("objects");

        File assetsFolder = new File(indexesFile.getParentFile(), "objects");

        File soundsOutputDirectory = new File(this.outputDirectory, "sounds");
        if (!soundsOutputDirectory.exists())
            soundsOutputDirectory.mkdir();

        AtomicInteger i = new AtomicInteger();
        objects.names().forEach(name -> {
            if (name.contains("sounds")) {
                i.getAndIncrement();
                String soundName = name.split("/")[name.split("/").length - 1];

                System.out.println("Processing " + soundName);


                JsonObject soundObject = (JsonObject) objects.get(name);

                String hash = soundObject.getString("hash", "null");
                if (hash.equalsIgnoreCase("null"))
                    return;

                String folder = hash.substring(0, 2);
                File soundFolder = new File(assetsFolder, folder);

                File soundFile = new File(soundFolder, hash);

                String[] subfolders = Arrays.stream(name.split("/")).filter(s -> !s.equalsIgnoreCase("minecraft") && !s.equalsIgnoreCase("sounds") && !s.contains(".ogg")).toArray(String[]::new);

                File lastFolder = soundsOutputDirectory;
                for (String subfolder : subfolders) {
                    lastFolder = new File(lastFolder, subfolder);
                    if (!lastFolder.exists())
                        lastFolder.mkdir();
                }

                try {
                    Files.copy(soundFile.toPath(), new File(lastFolder, soundName).toPath());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Exportation ended");
        alert.setHeaderText(null);
        alert.setContentText("Exportation finished ! " + i.get() + " sounds exported !");

        alert.showAndWait();
    }
}
